from dialog_to_animation.character import Character

def parse(source):
    '''
    Parses dialog contents into a list

    ea:
      ha
    fa:
      g
    ->
    [Character('ea'), Character('fa')], [(Character('ea'), 'ha'), (Character('fa'), 'g')]
    '''
    lines = source.split('\n') + [':']
    characters = {}
    current = None # the curent character object
    text = [] # the current lines for a character
    output = []
    for l in lines:
        l = l.rstrip()
        if l and l[0] != ' ' and l.endswith(':'):
            if text:
                output.append((current, '\n'.join(text)))
                text = []
            if l == ':': # end
                break
            name = l[:-1]
            if name in characters:
                current = characters[name]
            else:
                z = Character(name)
                characters[name] = z
                characters[name[0]] = z # TODO fix corner cases
                current = z
        else:
            t = l.lstrip()
            if t:
                text.append(t)
    
    return [v for k, v in characters.items() if len(k) > 1], output



